var grunt = require('grunt');

grunt.loadNpmTasks('grunt-svg2png');
grunt.loadNpmTasks('grunt-responsive-images');
grunt.loadNpmTasks('grunt-contrib-imagemin');
grunt.loadNpmTasks('grunt-contrib-clean');

grunt.initConfig({
  svg2png: {
    all: {
      // specify files in array format with multiple src-dest mapping
      files: [
        // rasterize all SVG files in "img" and its subdirectories to "img/png"
        { cwd: 'src/', src: ['**/*.svg'], dest: 'dist' }
      ]
    }
  },

  responsive_images: {
    png: {
      options: {
        rename: false,
        sizes: [{
          width: 64,
          height: 64,
          suffix: '-64'
        }, {
          width: 128,
          height: 128,
          suffix: '-128'
        }]
      },
      files: [{
        expand: true,
        cwd: 'dist/',
        src: '**/*.png',
        dest: 'dist/'
      }]
    }
  },

  imagemin: {
    svg: {
      files: [{
        expand: true,
        cwd: 'src/',
        src: '**/*.svg',
        dest: 'dist/'
      }]
    },
    png: {
      files: [{
        expand: true,
        cwd: 'dist/',
        src: '**/*.png',
        dest: 'dist/'
      }]
    }
  },

  clean: {
    dist: 'dist'
  }
});

grunt.task.registerTask('png', ['svg2png:all', 'responsive_images:png', 'imagemin:png']);
grunt.task.registerTask('svg', ['imagemin:svg']);
grunt.task.registerTask('default', ['clean:dist', 'svg', 'png']);
